package org.example;

import io.vertx.core.Vertx;


public class Main {
    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        Consumer consumer= new Consumer(vertx);
        System.out.println(consumer);
        //System.out.println("Main class for making a client");
        Producer producer= new Producer(vertx);
        System.out.println(producer);
    }
}