package org.example;

import io.vertx.core.Vertx;
import io.vertx.kafka.client.producer.KafkaProducer;
import io.vertx.kafka.client.producer.KafkaProducerRecord;

import java.util.HashMap;
import java.util.Map;

public class Producer {
    Producer(Vertx vertx){
        Map<String, String> config = new HashMap<>();
        config.put("bootstrap.servers", "localhost:9092");
        config.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        config.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        System.out.println("Class for the producer");
// use producer for interacting with Apache Kafka
        KafkaProducer<String, String> producer = KafkaProducer.create(vertx, config);
        for (int i = 0; i < 10; i++) {
            String message = "Test message " + i;
            KafkaProducerRecord<String, String> kafkaRecord = KafkaProducerRecord.create("test", message);
            producer.write(kafkaRecord, done -> {
                System.out.println("wo");
                if (done.succeeded()) {
                    System.out.println("Sent message: " + message);
                } else {
                    System.err.println("Failed to send message: " + done.cause().getMessage());
                }
            });

        }
    }
}