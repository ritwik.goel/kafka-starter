package org.example;

import io.vertx.core.Vertx;
import io.vertx.kafka.client.consumer.KafkaConsumer;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Consumer {
    Consumer(Vertx vertx){
        Map<String, String> consumerConfig = new HashMap<>();
        consumerConfig.put("bootstrap.servers", "localhost:9092");
        consumerConfig.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        consumerConfig.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        consumerConfig.put("group.id", "test");

// use consumer for interacting with Apache Kafka
        KafkaConsumer<String, String> consumer = KafkaConsumer.create(vertx, consumerConfig);
        //Consumer and producer
        System.out.println(consumer);
        consumer
                .subscribe("test")
                .onSuccess(v ->
                        System.out.println("subscribed")
                ).onFailure(cause ->
                        System.out.println("Could not subscribe " + cause.getMessage())
                );
        consumer.handler(record -> {
            System.out.println("Received message: " + record.value());
        });
    }
}
